/**
 * @author Andrew
 * Initiates program calling upon the CLI and nothing else.
 */
package AlphabeticalWordRanker;

public class Initializer {
    public static void main(String[] args) {
        CommandLineInterface r = new CommandLineInterface(args);
        System.exit(0);
    }
}
