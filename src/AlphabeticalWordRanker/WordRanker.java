/**
 * @author Andrew Carter
 * Date: 10/20/14 
 */
package AlphabeticalWordRanker;

/**Approximated at O(kn)
     * Process: 
     * 1.) Read in word
     * 2.) determine size of word
     * 3.) setup a special letterCount array
     * 4.) calculate the Rank
     * 5.) Calculating the rank is a loop that runs at approximately O(kn)
     *      5.1) Read char O(1)
     *      5.2) FactorialValue = factorialValue/remainingLetterCount O(1)
     *      5.3) Count the letters smaller than the current read letter by scanning through letterCount. O(k)
     *      5.4) Scan letterCount again for repeating characters with counts >= 2. O(k)
     *      5.5) Update rank with this new value added to it
     *      5.6) decrement letter count and remaining letter count
     * 6.) return rank */
public class WordRanker {
    
    public char [] word;
    private short size;
    private short[] letterCount;
    public long rank = 1;
    
    public WordRanker ( String str ) {
        word = str.toCharArray();
        size = (short)str.length();
        letterCount = new short[26];
        letterCount = buildLetterCountArray();
        rank = calculateRank();
    }
    
     /*An important optimization to bolster effeciency of algorithm
      Maps letters in array in an alphabetical fashion such that the indexes are:
        1.) A = 0
        2.) B = 1
           ...
        26.) Z = 25*/
    private short [] buildLetterCountArray() {
        for ( short idx=0; idx < size; idx++) {
            short relativeIndex  = ((short)(word[idx]-65)); //ASCII mapping
            letterCount[relativeIndex]++; 
        }
        return letterCount;
    }
    
    /*Where the mathematical computation happens for the ranker.
     *  5.) Loop through input string: O(kn) algorithm
     *      5.1) Read char O(1)
     *      5.2) FactorialValue = factorialValue/remainingLetterCount O(1)
     *      5.3) Count the letters smaller than the current read letter by scanning through letterCount. O(k)
     *      5.4) Scan letterCount again for repeating characters with counts >= 2. O(k)
     *      5.5) Update rank with this new value added to it
     *      5.6) decrement letter count and remaining letter count
     *  6.) return rank */
    public long calculateRank() {
        Factorial factorialBase = new Factorial((short)(word.length));
        Factorial factorialDivisor;
        long factorialInteger;
        int remainingLetterCount = size;
        
        for ( short idx = 0; idx < size; idx++ ) {
            char letterAnalyzed = word[idx];
            short letterIndex = (short)(letterAnalyzed-65);
            //System.out.println(c);
            factorialBase.val = factorialBase.val/(long)remainingLetterCount;
            
            //Scan for all letters smaller than the one being analyzed
            short lettersSmallerThanChar = 0;
            for ( short k = (short)(letterIndex-1); k >= 0; k--) {
                lettersSmallerThanChar += letterCount[k];
            }
            
            if ( lettersSmallerThanChar > 0 ) {
                //Scan for repeating characters and divide by their factorial count
                factorialInteger = factorialBase.val;
                for ( short j = 0; j < letterCount.length; j++) {
                    if ( letterCount[j] >= 2 ) {
                        factorialDivisor = new Factorial( letterCount[j] );
                        factorialInteger = factorialInteger/factorialDivisor.val;
                    }
                }
                //Update your rank
                rank += (lettersSmallerThanChar * factorialInteger);
            }
            
            //decrement count of letter being analyzed
            letterCount[letterIndex]--;
            remainingLetterCount--;
        }
        
        return rank;
    }
    
    public String printRank() {
        return ("Rank is : " + rank);
    }
}
