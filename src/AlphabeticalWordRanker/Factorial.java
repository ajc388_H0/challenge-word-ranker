/**
 * @author Andrew
 * Generic factorial solution
 */
package AlphabeticalWordRanker;

import java.math.BigInteger;

public class Factorial {
    
    public long val;
    
    public Factorial(short num) {
        val = factorial(num);
    }
    
    public Factorial() {}
    
    public long factorial(short num) throws Error {
        if ( num < 0 ) {
            throw new Error("Factorial defined for positive integers");
        }
        long val = 1;
        for ( long n = 1; n <= (long)num; n++) {
           val = n*val;
        }
        return val;
    }
    
    
}