/**
 * @author Andrew
 * A basic command line interface with input validation.
 */
package AlphabeticalWordRanker;
import java.util.regex.Pattern;

public class CommandLineInterface {
    
    public CommandLineInterface() {   }
    public CommandLineInterface ( String [] args ) {
        System.out.println("This is my implementation of the programming challenge for AthenaHealth.\n"
                + "It takes a word no longer than 20 characters and ranks it\n"
                + "as though it were an alphabetically sorted aniagram.\n");
        
        if ( validateArgLength(args) && validateInput(args[0])) {
            WordRanker r = new WordRanker(args[0]);
            System.out.println(r.printRank());
            
        } else {
            System.out.println("Program failed to execute.");
            System.exit(1);
        }
    }
        
    public Boolean validateArgLength(String [] args) {
        if ( args.length != 1 ) {
            System.out.println("You must enter one word as an argument.");
            return false;
        } else {
            return true;
        }
    }
    
    public Boolean validateInput(String str) {
        Pattern p = Pattern.compile("[A-Z]{1,20}");
        if ( !p.matcher(str).matches() ) {
            System.out.println("You may only use 1-20 upper case letters.");
            return false;
        } else {
            return true;
        }
    }
}

