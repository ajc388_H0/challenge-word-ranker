import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import AlphabeticalWordRanker.CommandLineInterface;

public class TestCommandLineInterface {
    public CommandLineInterface testUI;
            
    @Before
    public void setUp() {
        testUI = new CommandLineInterface();
    }
    
    @After
    public void tearDown() {
        testUI = null;
    }
    
    //Testing block for testing the validateArgLength function
    @Test
    public void testArgLengthTooManyArgs() {
        String [] args = {"a", "cat"};
        assertFalse(testUI.validateArgLength(args));
    }
    
    @Test
    public void testArgLengthNoArgs() {
        String [] args = {};
        assertFalse(testUI.validateArgLength(args));
    }
    
    @Test
    public void testArgLength() {
        String [] args = {"You did it right!"};
        assertTrue(testUI.validateArgLength(args));
    }
        
    //Test block for the regular expression validator, validateUpperCaseLetters function
    @Test
    public void testLowerCaseLetters() {
        assertFalse(testUI.validateInput("abc"));
    }
    
    @Test
    public void testSpecialCharacters() {
        assertFalse(testUI.validateInput("!@#$%^&*()-="));
    }
    
    @Test
    public void testNumbers() {
        assertFalse(testUI.validateInput("1234567890"));
    }
    
    @Test
    public void testUpperCaseLetters() {
        assertTrue(testUI.validateInput("ABCDEFGHIJKLMNOPQRST"));
    }
    
    @Test
    public void testUpperCaseLettersTooLarge() {
        assertFalse(testUI.validateInput("ABCDEFGHIJKLMNOPQRSTU"));
    }
    
    @Test
    public void testEmptyString() {
        assertFalse(testUI.validateInput(""));
    }
}
