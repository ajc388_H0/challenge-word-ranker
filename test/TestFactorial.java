import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import AlphabeticalWordRanker.Factorial;
import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;

public class TestFactorial extends AbstractBenchmark {
    
    public Factorial test;
    
    public TestFactorial() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        test = new Factorial();
    }
    
    @After
    public void tearDown() {
        test = null;
    }
    
    @Test (expected = Error.class)
    public void testNegativeOne() {
        test.factorial((short)-1);
    }
    
    @Test
    public void testZero() {
        assertEquals((long)1, test.factorial((short)0));
    }
    
    @Test
    public void testOne() {
        assertEquals((long)1, test.factorial((short)1));
    }
    
    @Test
    public void testTwo() {
        assertEquals((long)2, test.factorial((short)2));
    }
    
    @Test
    public void testThree() {
        assertEquals((long)6, test.factorial((short)3));
    }
    
    @Test
    public void testFour() {
        assertEquals((long)24, test.factorial((short)4));
    }
    
    @Test
    public void testFive() {
        assertEquals((long)120, test.factorial((short)5));
    }
    
    @Test
    public void testTen() {
        assertEquals((long)3628800, test.factorial((short)10));
    }
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testTwenty() {
        assertEquals(2432902008176640000L, test.factorial((short)20));
    }
}
