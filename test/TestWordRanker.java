import org.junit.Test;
import static org.junit.Assert.*;
import AlphabeticalWordRanker.WordRanker;
import org.junit.After;
import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;

public class TestWordRanker extends AbstractBenchmark {
    
    private WordRanker wordRanker;
    public TestWordRanker() {}
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @After
    public void tearDown() {
        wordRanker = null;
    }
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerABAB() {
        wordRanker = new WordRanker("ABAB");
        assertEquals(2, wordRanker.rank);
    }
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerAAAB() {
        wordRanker = new WordRanker("AAAB");
        assertEquals(1, wordRanker.rank);
    }
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerBAAA() {
        wordRanker = new WordRanker("BAAA");
        assertEquals(4, wordRanker.rank);
    }
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerQUESTION() {
        wordRanker = new WordRanker("QUESTION");
        assertEquals(24572, wordRanker.rank);
    }
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerBOOKKEEEPER() {
        wordRanker = new WordRanker("BOOKKEEPER");
        assertEquals(10743, wordRanker.rank);
    }
  
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerTEST() {
        wordRanker = new WordRanker("TEST");
        assertEquals(7, wordRanker.rank);
    }
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerT() {
        wordRanker = new WordRanker("T");
        assertEquals(1, wordRanker.rank);
    }
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerABCDEFGHIJQLMNOPQRSTUVWXY() {
        wordRanker = new WordRanker("ABCDEFGHIJKLMNOPQRSTUVWXY");
        assertEquals(1, wordRanker.rank);
    }
    
    /*Does not calculate correctly*/
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerZBCDEFGHIJQLMNOPQRSTUVWXY() {
        //should be 14890761641597746544640000, which is out of the scope of a long
        wordRanker = new WordRanker("ZBCDEFGHIJKLMNOPQRSTUVWXY");
        assertEquals(1, wordRanker.rank);
    }
    
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerABCDEFGHIJQLMNOPQRST() {
        wordRanker = new WordRanker("ABCDEFGHIJKLMNOPQRST");
        assertEquals(1, wordRanker.rank);
    }
    
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 0)
    @Test
    public void testRankerABCDEFGHIJQLMNOPSQRT() {
        wordRanker = new WordRanker("ABCDEFGHIJKLMNOPSQRT");
        assertEquals(13, wordRanker.rank);
    }
}
